/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nontapat.shapeproject;

/**
 *
 * @author DELL
 */
public class Rectangle {

    private double width;
    private double length;

    public Rectangle(double width, double length) {
        this.width = width;
        this.length = length;
    }
 

    public void setWidth(double width) {
        if (width <= 0) {
            System.out.println("Error: Width must more than zero!!!!");
            return;
        }
        this.width = width;
    }

    public void setLength(double length) {
        if (length <= 0) {
            System.out.println("Error: Length must more than zero!!!!");
            return;
        }
        this.length = length;
    }

    public double calArea() {
        return width * length;
    }
    
    public double getWidth() {
        return width;
    }
    
    public double getLength() {
        return length;
    }

}
