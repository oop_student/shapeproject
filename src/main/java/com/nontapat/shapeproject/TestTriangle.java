/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nontapat.shapeproject;

/**
 *
 * @author DELL
 */
public class TestTriangle {
    public static void main(String[] args) {
        Triangle tri1 = new Triangle(3,4);
        System.out.println("Area of tri1(b = "+tri1.getB()+", h = "+tri1.getH()+") is "+tri1.calArea());
        tri1.setH(8.7);
        System.out.println("Area of tri1(b = "+tri1.getB()+", h = "+tri1.getH()+") is "+tri1.calArea());
        tri1.setH(0);
        System.out.println("Area of tri1(b = "+tri1.getB()+", h = "+tri1.getH()+") is "+tri1.calArea());
    }
}
