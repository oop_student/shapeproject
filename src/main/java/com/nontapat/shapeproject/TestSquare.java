/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nontapat.shapeproject;

/**
 *
 * @author DELL
 */
public class TestSquare {
    public static void main(String[] args) {
         Square sq1 = new Square(4);
         System.out.println("Area of sq1(side = "+sq1.getSide()+") is "+sq1.calArea());
         sq1.setSide(7.5);
         System.out.println("Area of sq1(side = "+sq1.getSide()+") is "+sq1.calArea());
         sq1.setSide(0);
         System.out.println("Area of sq1(side = "+sq1.getSide()+") is "+sq1.calArea());
    }
}
